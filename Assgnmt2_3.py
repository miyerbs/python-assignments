# This first line is provided for you

try:
	hrs = raw_input("Enter Hours:")
	hrs_value = float(hrs)
	rate = raw_input("Enter Rate per hour:")
	rate_value = float(rate)
except:
	print "Invalid input. Please enter numeric values."
	quit()
	
if hrs_value > 40 :
	gross_pay = 40 * rate_value + ((hrs_value - 40) * 1.5 * rate_value)
else :
	gross_pay = hrs_value * rate_value
print(gross_pay)