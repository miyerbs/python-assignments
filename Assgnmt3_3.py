score = raw_input("Enter Score: ")
try :
	score_value = float(score)
except:
	print("Score entered is not a numeric value.")
	quit ()

if score_value < 0.6:
	print "F"
elif score_value < 0.7:
	print "D"
elif score_value < 0.8:
	print "C"
elif score_value < 0.9:
	print "B"
elif score_value <= 1.0:
	print "A"
else :
	print("Score entered is greater than maximum value allowed. Enter a score less than 1.")
